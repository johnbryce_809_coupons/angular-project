import { Injectable } from '@angular/core';
import { CompanyBean } from 'src/app/models/CompanyBean';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LogInBean } from 'src/app/models/logInBean';
import { UtilService } from '../util.service';

@Injectable({
  providedIn: 'root'
})
export class CompanyApiService {

  constructor( private http : HttpClient, private utilService : UtilService) { }
  
  getCompanyData(companyId): Observable<CompanyBean> {
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
    var url = this.utilService.webServiceUrl + "/rest/companies/"+ companyId;
    return this.http.get<CompanyBean>(url,{ withCredentials: true, headers: headers });
  }

  getAllCompanies(): Observable<CompanyBean[]> {
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
    var url = this.utilService.webServiceUrl + "/rest/companies/";
    return this.http.get<CompanyBean[]>(url,{ withCredentials: true, headers: headers });
  }
  createCompany(companyBean:CompanyBean): Observable<Number> {
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
    return this.http.post<Number>(this.utilService.webServiceUrl + "/rest/companies",companyBean,{ withCredentials: true, headers: headers });
  }
 
  updateCompany(companyBean:CompanyBean): Observable<void> {
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
    return this.http.put<void>(this.utilService.webServiceUrl + "/rest/companies",companyBean,{ withCredentials: true, headers: headers });
  }
 
  updateCompanyPassword(passwordBean:LogInBean): Observable<void> {
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
    var url = this.utilService.webServiceUrl + "/rest/companies/"+ passwordBean.userId + "/password";
    return this.http.put<void>(url,passwordBean,{ withCredentials: true, headers: headers});
  }

  deleteCompany(companyId): Observable<void> {
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
    var url = this.utilService.webServiceUrl + "/rest/companies/"+ companyId;
    return this.http.delete<void>(url,{ withCredentials: true , headers: headers});
  }


}
