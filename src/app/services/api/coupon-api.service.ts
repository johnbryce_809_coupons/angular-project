import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Coupon } from 'src/app/models/coupon';
import { CartBean } from 'src/app/models/cartBean';
import { HttpClient, HttpEvent, HttpHeaders } from '@angular/common/http';
import { UtilService } from '../util.service';

@Injectable({
  providedIn: 'root'
})
export class CouponApiService {
  
  constructor( private http : HttpClient, private utilService : UtilService) { }
  public getCoupons():Observable<Coupon[]>{
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
    return this.http.get<Coupon[]>(this.utilService.webServiceUrl + "/rest/coupons",{ withCredentials: true, headers: headers });
  }
  public createCoupon(coupon : Coupon):Observable<number>{
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
    // return this.http.post<number>(this.utilService.webServiceUrl + "/rest/coupons",coupon,{ withCredentials: true ,headers: headers});
    return this.http.post<number>(this.utilService.webServiceUrl + "/rest/coupons",coupon,{ withCredentials: true, headers: headers });
  }
  public uploadImage(uploadData:FormData):Observable<HttpEvent<void>>{
    // return this.http.post<number>(this.utilService.webServiceUrl + "/rest/coupons",coupon,{ withCredentials: true });
     return this.http.post<void>(this.utilService.webServiceUrl + "/rest/coupons/uploadimage", uploadData,{ withCredentials: true, reportProgress: true,
      observe: 'events' });
  }
  public updateCoupon(coupon : Coupon):Observable<void>{
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
    return this.http.put<void>(this.utilService.webServiceUrl + "/rest/coupons",coupon,{ withCredentials: true , headers: headers });
  }
  public getCouponsByType(type:string):Observable<Coupon[]>{
    var urlString = this.utilService.webServiceUrl + "/rest/coupons/couponType?couponType="+type;
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
    return this.http.get<Coupon[]>(urlString,{ withCredentials: true , headers: headers });
  }
  public getCompanyCoupons(companyId : Number):Observable<Coupon[]>{
    let restUrl = this.utilService.webServiceUrl + "/rest/coupons/company/" + companyId;
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
    return this.http.get<Coupon[]>(restUrl,{ withCredentials: true , headers: headers });
  }
  public getCompanyCouponsByType(companyId:number, type:string):Observable<Coupon[]>{
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
    var urlString = this.utilService.webServiceUrl + "/rest/coupons/company/"+companyId+"/couponType?couponType="+type;
    return this.http.get<Coupon[]>(urlString,{ withCredentials: true , headers: headers });
  }
  public getCompanyCouponsByPrice(companyId:number, price:number):Observable<Coupon[]>{
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
    var urlString = this.utilService.webServiceUrl + "/rest/coupons/company/"+companyId+"/couponPrice?couponPrice="+price;
    return this.http.get<Coupon[]>(urlString,{ withCredentials: true , headers: headers });
  }
  public getCompanyCouponsByDate(companyId:number, date:Date):Observable<Coupon[]>{
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
    var urlString = this.utilService.webServiceUrl + "/rest/coupons/company/"+companyId+"/expirationDate?expirationDate="+date;
    return this.http.get<Coupon[]>(urlString,{ withCredentials: true , headers: headers });
  }
  public getCustomerCoupons(customerId : Number):Observable<Coupon[]>{
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
    let restUrl = this.utilService.webServiceUrl + "/rest/coupons/customer/" + customerId;
    return this.http.get<Coupon[]>(restUrl,{ withCredentials: true , headers: headers });
  }

  public getCustomerCouponsByType(customerId:number, type:string):Observable<Coupon[]>{
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
    var urlString = this.utilService.webServiceUrl + "/rest/coupons/customer/"+customerId+"/couponType?couponType="+type;
    return this.http.get<Coupon[]>(urlString,{ withCredentials: true , headers: headers });
  }

  public getCustomerCouponsByPrice(customerId:number, price:number):Observable<Coupon[]>{
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
    var urlString = this.utilService.webServiceUrl + "/rest/coupons/customer/"+customerId+"/couponPrice?couponPrice="+price;
    return this.http.get<Coupon[]>(urlString,{ withCredentials: true , headers: headers });
  }
  public purchaseCoupon(coupon: Coupon, customerId : Number): Observable<void> {
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
    var str = this.utilService.webServiceUrl + "/rest/coupons/"+ coupon.couponId + "/"+ customerId;
    return this.http.put<void>(str,{},{ withCredentials: true , headers: headers });
  }

  public checkoutCart(cartBean: CartBean, customerId : Number): Observable<CartBean> {
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
    var str = this.utilService.webServiceUrl + "/rest/coupons/cart/"+ customerId;
    return this.http.put<CartBean>(str,cartBean,{ withCredentials: true , headers: headers });
  }

  public deleteCoupon(couponId:number): Observable<void> {
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
    var url = this.utilService.webServiceUrl + "/rest/coupons/"+ couponId;
    return this.http.delete<void>(url,{ withCredentials: true , headers: headers });
  }
  public updateCouponAmount(amount:number, couponId : Number): Observable<void> {
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
    var str = this.utilService.webServiceUrl + "/rest/coupons/amount/"+ couponId + "?amount=" + amount;
    return this.http.put<void>(str,{},{ withCredentials: true , headers: headers });
  }
  
}
