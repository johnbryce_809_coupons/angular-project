import { Injectable } from '@angular/core';
import { LogInBean } from 'src/app/models/logInBean';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { promise } from 'protractor';
import { UtilService } from '../util.service';

@Injectable({
  providedIn: 'root'
})
export class LoginApiService {

  constructor( private http : HttpClient, private utilService : UtilService) { }

  public login(logInBean: LogInBean): Observable<Number> {
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
    return this.http.post<Number>(this.utilService.webServiceUrl + "/rest/login", logInBean, { withCredentials: true, headers: headers });
  }

  public logout() {
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
   return this.http.delete<void>(this.utilService.webServiceUrl + "/rest/logout", { withCredentials: true, headers: headers });
  }

  public check(): Promise<LogInBean> {
    const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
    return this.http.get<LogInBean>(this.utilService.webServiceUrl + "/rest/check", { withCredentials: true, headers: headers }).toPromise();
  }
}
